# My Home Assistant Setup
This repo contains my current [Home Assistant](https://www.home-assistant.io/) configuration and my Dashboard config. Below is some information about the devices I use in my Smart Home and how they are connected to Home Assistant.

**Website:** [My Blog](https://atxcoder.github.io/) (a work-in-progress)

## 💡 Lights
|Device|Purpose|HA Integration
|---|---|---|
|Phillips Hue|Have a number of Hue White Lamp bulbs in lamps, dining room, bathroom, etc. These bulbs are handy as many of my light fixtures do not have a neutral wire.|[Phillips Hue Integration](https://www.home-assistant.io/integrations/hue)
|[WLED](https://kno.wled.ge/)|I have an ESP8266-D1 connected to a small strip of LED's in my office running WLED. WLED is a awesome firmware that lets you control NeoPixel LEDs and provides hundreds of effects, OTA updates, and a ton of other useful options. This summer I will be adding some to my pool outside for some cool night swimming!|[WLED Integration](https://www.home-assistant.io/integrations/wled)

## 🎬 Media
|Device|Purpose|HA Integration
|---|---|---|
|Roku|This is a great streaming device that you connect to your TV to instantly turn it into a Smart TV. It lets you watch content from providers such as Netflix, Disney+, Hulu and a ton more.
|[Plex](https://www.plex.tv/)|Plex is a media server that lets you store, categorize, and play all your media (Movies, TV, Music, Podcast,etc) on a number of different devices (you can even stream it to your Roku). Think of it as your own personal Netflix and Spotify all wrapped into one. Plus it's free.

## 📱 Smart Switches
|Device|Purpose|HA Integration
|---|---|--|
|[Shelly Relays](https://shelly.cloud/)|Shelly relays are great for situations where you want to make a dumb switch smart. The fit right behind your light switch (in most cases) but can also be wired near the light fixture itself if you don't have a neutral wire running to your light switch. I also have one of these spliced into the power cable for my Monoprice MP Select Mini 3D Printer so I can switch it on and off from within Home Assistant.|via MQTT, but HA also has a [Shelly integration](https://www.home-assistant.io/integrations/shelly/).
|Z-Wave switch|You generic, run of the mill Z-Wave light switch controls the lights in my wife's shop.|Z-WAve.JS

## 🔐Security
|Device|Purpose|HA Integration
|---|---|---|
|[Blue Iris](https://blueirissoftware.com/)|Blue Iris in a Network Video Recorder (NVR) application that allows you to monitor, record, and act on video streams coming from networked cameras. I have a number of POE cameras that feed into Blue Iris. The application monitors those feeds for motion and then alerts Home Assistant via MQTT when motion is detected.|Connected via MQTT

## Other
|Device|Purpose|HA Integration
|---|---|--|
|Ecovac|This little robotic vacuum does a great job at keeping our floors nice and clean. Home Assistant has a integration for it.|[Ecovac Integration](https://www.home-assistant.io/integrations/ecovacs/)
|[Octoprint](https://octoprint.org/)|Integration that connects to my Octopring server and provides a number sensors such as job percentage, remaining run time, and printer status. I use it to run some automations such as turning my 3-D printer off once a job has been completed.|[Ocotoprint](https://www.home-assistant.io/integrations/octoprint/)